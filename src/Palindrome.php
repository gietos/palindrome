<?php

namespace App;

class Palindrome
{
    /**
     * @param string $word
     * @return bool
     */
    public static function isPalindrome($word)
    {
        $len = mb_strlen($word);
        $middle = floor($len / 2);

        for ($i = 0; $i < $middle; $i++) {
            $leftLetter = mb_convert_case(mb_substr($word, $i, 1), MB_CASE_LOWER);
            $rightLetter = mb_convert_case(mb_substr($word, $len - 1 - $i, 1), MB_CASE_LOWER);

            if ($leftLetter != $rightLetter) {
                return false;
            }
        }

        return true;
    }
}
