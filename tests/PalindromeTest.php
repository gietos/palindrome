<?php

class PalindromeTest extends \PHPUnit_Framework_TestCase
{
    public function dataProvider()
    {
        return [
            ['Deleveled', true],
            ['adada', true],
            ['adadc', false],
            ['affa', true],
            ['affc', false],
        ];
    }

    /**
     * @dataProvider dataProvider
     */
    public function testPalindrome($input, $result)
    {
        return $this->assertEquals($result, \App\Palindrome::isPalindrome($input));
    }
}
